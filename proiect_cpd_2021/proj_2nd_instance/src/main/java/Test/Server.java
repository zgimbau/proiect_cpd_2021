package Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Server {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private final Client myClient;
    private Subscriber subscriber;
    private ArrayList<String> messages;

    public Server(Client c1){
        this.myClient = c1;
        this.messages = new ArrayList<>();
    }

    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        clientSocket = serverSocket.accept();
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        ArrayList<String> topics = new ArrayList<>();
        topics.add("sport");
        topics.add("news");
        subscriber = new Subscriber(topics, messages);
        subscriber.start();
        Publisher publisher = new Publisher(topics, messages);
        publisher.start();

        while(true){
            String token = in.readLine();
            if("Token".equals(token)){

                publisher.setPublish(true);
                System.out.println("\n<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>");
                System.out.println("<<Token received... forwarding in 10 seconds!>>\n");
                System.out.println("Subscribed to ");
                for(String topic: topics){
                    System.out.println(topic);
                }
                System.out.println("Publish with <<topic>> <<message>> form:\n");
                try {
                    TimeUnit.SECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                publisher.setPublish(false);
                messages.clear();
                myClient.sendMessage(token);
                System.out.println("<<Token sent!>>\n\n");
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>");
            }
        }
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
        serverSocket.close();
    }

}

package Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


public class Client extends Thread{
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    @Override
    public void run(){
        while(true){
            try{
                startConnection("127.0.0.1", 8080);
                break;
            }
            catch (IOException ioException) {
            }
        }
    }

    public void startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        System.out.println("<<Succesfully connected to server 1!>>\n\n");
    }

    public void sendMessage(String msg) throws IOException {
        out.println(msg);
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }
}

package Test;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.nio.Buffer;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Publisher extends Thread{
    private final String X_NAME = "topic_logs";
    private Scanner scanner;
    private BufferedReader reader;
    private boolean publishing = false;
    private ArrayList<String> messages;
    private ArrayList<String> topics;
    public Publisher(ArrayList<String> topics, ArrayList<String> messages){
        this.messages = messages;
        this.topics = topics;
    }

    @Override
    public void run(){
        String uri = System.getenv("CLOUDAMQP_URL");
        if(uri == null) uri = "amqp://guest:guest@localhost";

        ConnectionFactory factory = new ConnectionFactory();
        try {
            factory.setUri(uri);
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
        factory.setConnectionTimeout(30000);

        scanner = new Scanner(System.in);


        try(Connection connection = factory.newConnection();
            Channel channel = connection.createChannel()){
            channel.exchangeDeclare(X_NAME, "topic");
            String routingKey;
            String message;
            while(true){
                String cmd = scanner.nextLine();
                String[] words = cmd.split("\\s");
                String topic = words[0];
                if(publishing){
                    if(topics.contains(topic)){
                        routingKey = topic;
                        message = cmd.substring(topic.length()+1);
                        messages.add(message);

                        channel.basicPublish(X_NAME, routingKey, null, message.getBytes(StandardCharsets.UTF_8));
                        System.out.println("<<Message sent to the " + topic + " topic>>");
                    }
                    else{
                        System.out.println("<<You cannot post to this topic>>");
                    }

                }
                else{
                    System.out.println("<<You cannot publish yet>>");
                }

            }

        } catch (IOException | TimeoutException ioException) {
            ioException.printStackTrace();
        }
    }

    public void setPublish(boolean p){
        this.publishing = p;
    }
}

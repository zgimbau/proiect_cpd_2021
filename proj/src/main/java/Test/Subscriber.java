package Test;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

public class Subscriber extends Thread{
    private final String X_NAME = "topic_logs";
    private ArrayList<String> sentMessages = new ArrayList<>();
    private ArrayList<String> topics;
    public Subscriber(ArrayList<String> topics, ArrayList<String> messages){
        this.topics = topics;
        this.sentMessages = messages;
    }

    @Override
    public void run(){

        try {
            String uri = System.getenv("CLOUDAMQP_URL");
            if(uri == null) uri = "amqp://guest:guest@localhost";

            ConnectionFactory factory = new ConnectionFactory();
            try {
                factory.setUri(uri);
            } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException e) {
                e.printStackTrace();
            }
            factory.setConnectionTimeout(30000);

            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.exchangeDeclare(X_NAME, "topic");
            String queueName = channel.queueDeclare().getQueue();

            for(String topic : topics){
                channel.queueBind(queueName, X_NAME, topic);
            }


            System.out.println("Waiting for messages...");
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                if(!sentMessages.contains(message))
                    System.out.println(delivery.getEnvelope().getRoutingKey() +": " + message);
            };

            channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {});

        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
}

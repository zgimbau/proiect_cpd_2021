package com.Proiect.proj;

import Test.Client;
import Test.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;




@SpringBootApplication
public class ProjApplication {

	public static void main(String[] args) throws IOException {

		SpringApplication.run(ProjApplication.class, args);
		Client c1 = new Client();
		c1.start();
		Server s1 = new Server(c1);
		s1.start(8080);
	}

}

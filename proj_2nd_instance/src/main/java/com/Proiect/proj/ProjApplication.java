package com.Proiect.proj;

import Test.Client;
import Test.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;




@SpringBootApplication
public class ProjApplication {

	public static void main(String[] args) throws IOException {

		SpringApplication.run(ProjApplication.class, args);
		Client c2 = new Client();
		c2.start();
		Server s2 = new Server(c2);
		s2.start(8081);
	}

}
